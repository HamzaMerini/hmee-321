close all
clear all
clc
set(0,'DefaultFigureWindowStyle','docked') 

R=1e-6;r=0.5e-6;
rho=r/R;

% MGD 
%ne marche pas tres bien %jouer sur le e-
l1=2.5e-6;
l2=2.2e-6;
l3=2.2e-6;

pos=MGD(l1,l2,l3,R,r);

xc=pos(1);
yc=pos(2);
zc=pos(3);
phi=pos(4);
beta=pos(5);
psi=pos(6);

qd=[xc;yc;zc;phi;beta;psi]

%qd %e-7
% xc=0e-7;
% yc=0e-7;
% zc=8e-7;
% phi=0;
% beta=0;
% psi=0;

trans=[xc,yc,zc]';

mR=[cos(beta)*cos(psi)               -cos(beta)*sin(psi)               sin(beta);
    cos(phi)*sin(psi)+sin(phi)*sin(beta)*cos(psi)     cos(phi)*cos(psi)-sin(phi)*sin(beta)*sin(psi)     -sin(phi)*cos(beta);
    sin(phi)*sin(psi)-cos(phi)*sin(beta)*cos(psi)     sin(phi)*cos(psi)+cos(phi)*sin(beta)*sin(psi)    cos(phi)*cos(beta)];
        
nx=mR(1,1);        
ny=mR(2,1);        
nz=mR(3,1);        

ox=mR(1,2);        
oy=mR(2,2);        
oz=mR(3,2);       
        
T=[mR trans;
    0 0 0 1];

Xc=xc/R; Yc=yc/R;Zc=zc/R;

Xc=subs(Xc);
Xc=cast(Xc,'double');
Yc=subs(Yc);
Yc=cast(Yc,'double');
Zc=subs(Zc);
Zc=cast(Zc,'double');

L1=sqrt((nx*rho+Xc-1)^2+(ny*rho+Yc)^2+(nz*rho+Zc)^2);
L2=sqrt(0.25*( (-nx*rho+sqrt(3)*ox*rho+2*Xc+1)^2+ (-ny*rho+sqrt(3)*oy*rho+2*Yc-sqrt(3))^2+...
    (-nz*rho+sqrt(3)*oz*rho+2*Zc)^2));
L3=sqrt(0.25*((-nx*rho-sqrt(3)*ox*rho+2*Xc+1)^2+ ...
(-ny*rho-sqrt(3)*oy*rho+2*Yc+sqrt(3))^2 + (-nz*rho-sqrt(3)*oz*rho+2*Zc)^2));

 
l1=L1*R
l2=L2*R
l3=L3*R


%% Dessin
figure(1)
clf
Pp1=[R;0;0];
plot3(Pp1(1),Pp1(2),Pp1(3),'*k','LineWidth',3)
hold on
text(Pp1(1),Pp1(2),Pp1(3),'  Pp1')

Pp2=[-R/2;R*(sqrt(3)/2);0];
plot3(Pp2(1),Pp2(2),Pp2(3),'*k','LineWidth',3)
hold on
text(Pp2(1),Pp2(2),Pp2(3),'  Pp2')

Pp3=[-R/2;-R*(sqrt(3)/2);0];
plot3(Pp3(1),Pp3(2),Pp3(3),'*k','LineWidth',3)
hold on
text(Pp3(1),Pp3(2),Pp3(3),'  Pp3')

Ppx=[Pp1(1) Pp2(1) Pp3(1) Pp1(1)];
Ppy=[Pp1(2) Pp2(2) Pp3(2) Pp1(2)];
Ppz=[Pp1(3) Pp2(3) Pp3(3) Pp1(3)];

plot3(Ppx,Ppy,Ppz,'Linewidth',3)

Pb1=[r;0;0];
Pb1_=T*[Pb1;1];
hold on
plot3(Pb1_(1),Pb1_(2),Pb1_(3),'*k','LineWidth',3)
hold on
text(Pb1_(1),Pb1_(2),Pb1_(3),'  Pb1')

Pb2=[-r/2;r*(sqrt(3)/2);0];
Pb2_=T*[Pb2;1];
hold on
plot3(Pb2_(1),Pb2_(2),Pb2_(3),'*k','LineWidth',3)
hold on
text(Pb2_(1),Pb2_(2),Pb2_(3),'  Pb2')

Pb3=[-r/2;-r*(sqrt(3)/2);0];
Pb3_=T*[Pb3;1];
hold on
plot3(Pb3_(1),Pb3_(2),Pb3_(3),'*k','LineWidth',3)
hold on
text(Pb3_(1),Pb3_(2),Pb3_(3),'  Pb3')


Pbx=[Pb1_(1) Pb2_(1) Pb3_(1) Pb1_(1)];
Pby=[Pb1_(2) Pb2_(2) Pb3_(2) Pb1_(2)];
Pbz=[Pb1_(3) Pb2_(3) Pb3_(3) Pb1_(3)];

hold on
plot3(Pbx,Pby,Pbz,'g','Linewidth',3)

Z1x=[Pp1(1) Pb1_(1)];
Z1y=[Pp1(2) Pb1_(2)];
Z1z=[Pp1(3) Pb1_(3)];

Z2x=[Pp2(1) Pb2_(1)];
Z2y=[Pp2(2) Pb2_(2)];
Z2z=[Pp2(3) Pb2_(3)];

Z3x=[Pp3(1) Pb3_(1)];
Z3y=[Pp3(2) Pb3_(2)];
Z3z=[Pp3(3) Pb3_(3)];

hold on
plot3(Z1x,Z1y,Z1z,'r','Linewidth',3)
hold on
plot3(Z2x,Z2y,Z2z,'r','Linewidth',3)
hold on
plot3(Z3x,Z3y,Z3z,'r','Linewidth',3)

grid on
% view([1,1,1]);
% axis equal;
xlim([-15 15]*1e-7) 
ylim([-15 15]*1e-7) 
zlim([0 30]*1e-7) 


%% Centre du triangle haut
syms X Y Z
eq1=(Pb3_(1)+Pb1_(1))/2+(Pb2_(1)+Pb1_(1))/2+(Pb2_(1)+Pb3_(1))/2==3*X;
eq2=(Pb3_(2)+Pb1_(2))/2+(Pb2_(2)+Pb1_(2))/2+(Pb2_(2)+Pb3_(2))/2==3*Y;
eq3=(Pb3_(3)+Pb1_(3))/2+(Pb2_(3)+Pb1_(3))/2+(Pb2_(3)+Pb3_(3))/2==3*Z;

solu=solve(eq1,eq2,eq3,X,Y,Z); 
hold on
plot3(solu.X,solu.Y,solu.Z,'*k','Linewidth',6)

hold on
plot3(xc,yc,zc,'*r','linewidth',3)

xlabel('X', 'FontSize', 15);
ylabel('Y', 'FontSize', 15);
zlabel('Z', 'FontSize', 15);


%% OT V2 marche
T1=[1.0000   -0.0000    0.0000   -0.0000
   -0.0000    1.0000    0.0000    0.0000
   -0.0000   -0.0000    1.0000    0.0000
         0         0         0    1.0000];
     
POT=[xc;yc;zc];
POT=T*[POT;1];
hold on
plot3([xc POT(1)],[yc POT(2)],[zc POT(3)],'-k','linewidth',3)



%% Forward Kinematics %probleme avec solve bloque le programme 
syms t1 t2 t3

eqq1=L1^2+L2^2+3-3*rho^2+L1*L2*cos(t1)*cos(t2)-2*L1*L2*sin(t1)*sin(t2)-3*L1*cos(t1)-3*L2*cos(t2)==0
eqq2=L2^2+L3^2+3-3*rho^2+L2*L3*cos(t2)*cos(t3)-2*L2*L3*sin(t2)*sin(t3)-3*L2*cos(t2)-3*L3*cos(t3)==0
eqq3=L3^2+L1^2+3-3*rho^2+L3*L1*cos(t3)*cos(t1)-2*L3*L1*sin(t3)*sin(t1)-3*L3*cos(t3)-3*L1*cos(t1)==0
[t1,t2,t3]=solve(eqq1,eqq2,eqq3,[t1 t2 t3],'Real',true)

% t1=t1(2)
% t2=t2(2)
% t3=t3(2)

%Coordinates of ball joints 
Xb1=1-L1*cos(t1);
Yb1=0;
Zb1=L1*sin(t1);
figure(2)
hold on
plot3(Xb1,Yb1,Zb1,'*y','linewidth',5)

Xb2=-0.5*(1-L2*cos(t2));
Yb2=(sqrt(3)/2)*(1-L2*cos(t2));
Zb2=L2*sin(t2);
hold on
plot3(Xb2,Yb2,Zb2,'*y','linewidth',5)

Xb3=-0.5*(1-L3*cos(t3));
Yb3=-(sqrt(3)/2)*(1-L3*cos(t3));
Zb3=L3*sin(t3);
hold on
plot3(Xb3,Yb3,Zb3,'*y','linewidth',5)
 
%Position of centroid
Xc=(Xb1+Xb2+Xb3)/R*3;
Yc=(Yb1+Yb2+Yb3)/R*3;
Zc=(Zb1+Zb2+Zb3)/R*3;

hold on
plot3([Xb1 Xb2 Xb3 Xb1],[Yb1 Yb2 Yb3 Yb1],[Zb1 Zb2 Zb3 Zb1],'-k','Linewidth',3)

hold on
plot3(Xc,Yc,Zc,'*m','linewidth',5)
grid on
view(-18,27)
