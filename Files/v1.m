close all
clear all
clc
set(0,'DefaultFigureWindowStyle','docked') 

%% Parametres 
R=10;r=5;l1=1;l2=4;l3=8;

figure(1)
clf
Pp1=[R;0;0];
plot3(Pp1(1),Pp1(2),Pp1(3),'*k','LineWidth',3)
hold on
text(Pp1(1),Pp1(2),Pp1(3),'  Pp1')

Pp2=[-R/2;R*sqrt(3)/2;0];
plot3(Pp2(1),Pp2(2),Pp2(3),'*k','LineWidth',3)
hold on
text(Pp2(1),Pp2(2),Pp2(3),'  Pp2')

Pp3=[-R/2;-R*sqrt(3)/2;0];
plot3(Pp3(1),Pp3(2),Pp3(3),'*k','LineWidth',3)
hold on
text(Pp3(1),Pp3(2),Pp3(3),'  Pp3')

Ppx=[Pp1(1) Pp2(1) Pp3(1) Pp1(1)];
Ppy=[Pp1(2) Pp2(2) Pp3(2) Pp1(2)];
Ppz=[Pp1(3) Pp2(3) Pp3(3) Pp1(3)];

plot3(Ppx,Ppy,Ppz,'Linewidth',3)

Pb1=[r;0;l1];
hold on
plot3(Pb1(1),Pb1(2),Pb1(3),'*k','LineWidth',3)
hold on
text(Pb1(1),Pb1(2),Pb1(3),'  Pb1')

Pb2=[-r/2;r*sqrt(3)/2;l2];
hold on
plot3(Pb2(1),Pb2(2),Pb2(3),'*k','LineWidth',3)
hold on
text(Pb2(1),Pb2(2),Pb2(3),'  Pb2')

Pb3=[-r/2;-r*sqrt(3)/2;l3];
hold on
plot3(Pb3(1),Pb3(2),Pb3(3),'*k','LineWidth',3)
hold on
text(Pb3(1),Pb3(2),Pb3(3),'  Pb3')


Pbx=[Pb1(1) Pb2(1) Pb3(1) Pb1(1)];
Pby=[Pb1(2) Pb2(2) Pb3(2) Pb1(2)];
Pbz=[Pb1(3) Pb2(3) Pb3(3) Pb1(3)];

hold on
plot3(Pbx,Pby,Pbz,'g','Linewidth',3)

Z1x=[Pp1(1) Pb1(1)];
Z1y=[Pp1(2) Pb1(2)];
Z1z=[Pp1(3) Pb1(3)];

Z2x=[Pp2(1) Pb2(1)];
Z2y=[Pp2(2) Pb2(2)];
Z2z=[Pp2(3) Pb2(3)];

Z3x=[Pp3(1) Pb3(1)];
Z3y=[Pp3(2) Pb3(2)];
Z3z=[Pp3(3) Pb3(3)];

hold on
plot3(Z1x,Z1y,Z1z,'r','Linewidth',3)
hold on
plot3(Z2x,Z2y,Z2z,'r','Linewidth',3)
hold on
plot3(Z3x,Z3y,Z3z,'r','Linewidth',3)


%% Centre du triangle haut
syms X Y Z
eq1=(Pb3(1)+Pb1(1))/2+(Pb2(1)+Pb1(1))/2+(Pb2(1)+Pb3(1))/2==3*X;
eq2=(Pb3(2)+Pb1(2))/2+(Pb2(2)+Pb1(2))/2+(Pb2(2)+Pb3(2))/2==3*Y;
eq3=(Pb3(3)+Pb1(3))/2+(Pb2(3)+Pb1(3))/2+(Pb2(3)+Pb3(3))/2==3*Z;

solu=solve(eq1,eq2,eq3,X,Y,Z); 
hold on
plot3(solu.X,solu.Y,solu.Z,'*k','Linewidth',3)


%% OT
c=15;
syms xd yd zd
eq10=sqrt((xd-Pb1(1))^2+(yd-Pb1(2))^2+(zd-Pb1(3))^2)-c==0;
eq20=sqrt((xd-Pb2(1))^2+(yd-Pb2(2))^2+(zd-Pb2(3))^2)-c==0;
eq30=sqrt((xd-Pb3(1))^2+(yd-Pb3(2))^2+(zd-Pb3(3))^2)-c==0;
sol=solve(eq10,eq20,eq30,xd,yd,zd); 

xd=sol.xd;
yd=sol.yd;
zd=sol.zd;

OTx=[solu.X xd(2)];
OTy=[solu.Y yd(2)];
OTz=[solu.Z zd(2)];

hold on
plot3(OTx,OTy,OTz,'-k','Linewidth',3)

xlim([-10 10])
ylim([-10 10])
zlim([-10 10])

xlabel('X', 'FontSize', 15);
ylabel('Y', 'FontSize', 15);
zlabel('Z', 'FontSize', 15);


