close all
clear all
clc
set(0,'DefaultFigureWindowStyle','docked') 

syms xc yc zc

R=1e-6;r=1e-6;
Xc=xc/R; Yc=yc/R;Zc=zc/R;
rho=r/R;

% MGD 
%ne marche pas tres bien %jouer sur le e-
l1=1.8e-6;
l2=2.2e-6;
l3=0.8e-6;

pos=MGD(l1,l2,l3,R,r);

xc=pos(1);
yc=pos(2);
zc=pos(3);
phi=pos(4);
beta=pos(5);
psi=pos(6);

qd=[xc;yc;zc;phi;beta;psi]

%qd %e-7
xc=0e-7;
yc=0e-7;
zc=8e-7;
phi=0;
beta=pi/8;
psi=pi/5;

trans=[xc,yc,zc]';

mR=[cos(beta)*cos(psi)               -cos(beta)*sin(psi)               sin(beta);
    cos(phi)*sin(psi)+sin(phi)*sin(beta)*cos(psi)     cos(phi)*cos(psi)-sin(phi)*sin(beta)*sin(psi)     -sin(phi)*cos(beta);
    sin(phi)*sin(psi)-cos(phi)*sin(beta)*cos(psi)     sin(phi)*cos(psi)+cos(phi)*sin(beta)*sin(psi)    cos(phi)*cos(beta)];
        
nx=mR(1,1);        
ny=mR(2,1);        
nz=mR(3,1);        

ox=mR(1,2);        
oy=mR(2,2);        
oz=mR(3,2);       
        
T=[mR trans;
    0 0 0 1];

Xc=subs(Xc);
Xc=cast(Xc,'double');
Yc=subs(Yc);
Yc=cast(Yc,'double');
Zc=subs(Zc);
Zc=cast(Zc,'double');

L1=sqrt((nx*rho+Xc-1)^2+(ny*rho+Yc)^2+(nz*rho+Zc)^2);
L2=sqrt(0.25*( (-nx*rho+sqrt(3)*ox*rho+2*Xc+1)^2+ (-ny*rho+sqrt(3)*oy*rho+2*Yc-sqrt(3))^2+...
    (-nz*rho+sqrt(3)*oz*rho+2*Zc)^2));
L3=sqrt(0.25*((-nx*rho-sqrt(3)*ox*rho+2*Xc+1)^2+ ...
(-ny*rho-sqrt(3)*oy*rho+2*Yc+sqrt(3))^2 + (-nz*rho-sqrt(3)*oz*rho+2*Zc)^2));
 

% L1=subs(L1);
% L1=cast(L1,'double');
% L2=subs(L2);
% L2=cast(L2,'double');
% L3=subs(L3);
% L3=cast(L3,'double');
 
l1=L1*R
l2=L2*R
l3=L3*R




%% dessin
figure(1)
clf
Pp1=[R;0;0];
plot3(Pp1(1),Pp1(2),Pp1(3),'*k','LineWidth',3)
hold on
text(Pp1(1),Pp1(2),Pp1(3),'  Pp1')

Pp2=[-R/2;R*(sqrt(3)/2);0];
plot3(Pp2(1),Pp2(2),Pp2(3),'*k','LineWidth',3)
hold on
text(Pp2(1),Pp2(2),Pp2(3),'  Pp2')

Pp3=[-R/2;-R*(sqrt(3)/2);0];
plot3(Pp3(1),Pp3(2),Pp3(3),'*k','LineWidth',3)
hold on
text(Pp3(1),Pp3(2),Pp3(3),'  Pp3')

Ppx=[Pp1(1) Pp2(1) Pp3(1) Pp1(1)];
Ppy=[Pp1(2) Pp2(2) Pp3(2) Pp1(2)];
Ppz=[Pp1(3) Pp2(3) Pp3(3) Pp1(3)];

plot3(Ppx,Ppy,Ppz,'Linewidth',3)

Pb1=[r;0;0];
Pb1_=T*[Pb1;1];
hold on
plot3(Pb1_(1),Pb1_(2),Pb1_(3),'*k','LineWidth',3)
hold on
text(Pb1_(1),Pb1_(2),Pb1_(3),'  Pb1')

Pb2=[-r/2;r*(sqrt(3)/2);0];
Pb2_=T*[Pb2;1];
hold on
plot3(Pb2_(1),Pb2_(2),Pb2_(3),'*k','LineWidth',3)
hold on
text(Pb2_(1),Pb2_(2),Pb2_(3),'  Pb2')

Pb3=[-r/2;-r*(sqrt(3)/2);0];
Pb3_=T*[Pb3;1];
hold on
plot3(Pb3_(1),Pb3_(2),Pb3_(3),'*k','LineWidth',3)
hold on
text(Pb3_(1),Pb3_(2),Pb3_(3),'  Pb3')


Pbx=[Pb1_(1) Pb2_(1) Pb3_(1) Pb1_(1)];
Pby=[Pb1_(2) Pb2_(2) Pb3_(2) Pb1_(2)];
Pbz=[Pb1_(3) Pb2_(3) Pb3_(3) Pb1_(3)];

hold on
plot3(Pbx,Pby,Pbz,'g','Linewidth',3)

Z1x=[Pp1(1) Pb1_(1)];
Z1y=[Pp1(2) Pb1_(2)];
Z1z=[Pp1(3) Pb1_(3)];

Z2x=[Pp2(1) Pb2_(1)];
Z2y=[Pp2(2) Pb2_(2)];
Z2z=[Pp2(3) Pb2_(3)];

Z3x=[Pp3(1) Pb3_(1)];
Z3y=[Pp3(2) Pb3_(2)];
Z3z=[Pp3(3) Pb3_(3)];

hold on
plot3(Z1x,Z1y,Z1z,'r','Linewidth',3)
hold on
plot3(Z2x,Z2y,Z2z,'r','Linewidth',3)
hold on
plot3(Z3x,Z3y,Z3z,'r','Linewidth',3)

grid on
view([1,1,1]);
axis equal;
xlim([-15 15]*1e-7) 
ylim([-15 15]*1e-7) 
zlim([0 30]*1e-7) 


%% Centre du triangle haut
syms X Y Z
eq1=(Pb3_(1)+Pb1_(1))/2+(Pb2_(1)+Pb1_(1))/2+(Pb2_(1)+Pb3_(1))/2==3*X;
eq2=(Pb3_(2)+Pb1_(2))/2+(Pb2_(2)+Pb1_(2))/2+(Pb2_(2)+Pb3_(2))/2==3*Y;
eq3=(Pb3_(3)+Pb1_(3))/2+(Pb2_(3)+Pb1_(3))/2+(Pb2_(3)+Pb3_(3))/2==3*Z;

solu=solve(eq1,eq2,eq3,X,Y,Z); 
hold on
plot3(solu.X,solu.Y,solu.Z,'*k','Linewidth',6)

hold on
plot3(xc,yc,zc,'*r','linewidth',3)

xlabel('X', 'FontSize', 15);
ylabel('Y', 'FontSize', 15);
zlabel('Z', 'FontSize', 15);

%% OT marche pas
% c=11e-7;
% syms xd yd zd
% eq10=sqrt((xd-Pb1_(1))^2+(yd-Pb1_(2))^2+(zd-Pb1_(3))^2)-c==0;
% eq20=sqrt((xd-Pb2_(1))^2+(yd-Pb2_(2))^2+(zd-Pb2_(3))^2)-c==0;
% eq30=sqrt((xd-Pb3_(1))^2+(yd-Pb3_(2))^2+(zd-Pb3_(3))^2)-c==0;
% sol=solve(eq10,eq20,eq30,xd,yd,zd); 
% 
% xdd=abs(sol.xd);
% ydd=abs(sol.yd);
% zdd=abs(sol.zd);
% 
% OTx=[solu.X xdd(2)];
% OTx=cast(OTx,'double')
% OTy=[solu.Y ydd(2)];
% OTy=cast(OTy,'double')
% OTz=[solu.Z zdd(2)];
% OTz=cast(OTz,'double')
% 
% hold on
% plot3(OTx,OTy,OTz,'-k','Linewidth',3)

% xlim([-10 10])
% ylim([-10 10])
% zlim([-10 10])


%axis square


%% OT V2 marche
T1=[1.0000   -0.0000    0.0000   -0.0000
   -0.0000    1.0000    0.0000    0.0000
   -0.0000   -0.0000    1.0000    0.0000
         0         0         0    1.0000];
     
POT=[xc;yc;zc];
POT=T*[POT;1];
hold on
plot3([xc POT(1)],[yc POT(2)],[zc POT(3)],'-k','linewidth',3)



%% Forward Kinematics 
% eq=L1^2+L2^2+3-3*rho^2+L1*L2*cos(theta1)*cos(theta2)-2*L1*L2*sin(theta1)*sin(theta2)-3*L1*cos(theta1)-3*L2*cos(theta2)==0;
% eqq=L2^2+L3^2+3-3*rho^2+L2*L3*cos(theta2)*cos(theta3)-2*L2*L3*sin(theta2)*sin(theta3)-3*L2*cos(theta2)-3*L3*cos(theta3)==0;
% eqqq=L3^2+L1^2+3-3*rho^2+L3*L1*cos(theta3)*cos(theta1)-2*L3*L1*sin(theta3)*sin(theta1)-3*L3*cos(theta3)-3*L1*cos(theta1)==0;
 
% %Coordinates of ball joints 
% Xb1=1-L1*cos(theta1);Yb1=0;Zb1=L1*sin(theta1);
% Xb2=-0.5*(1-L2*cos(theta2));Yb2=(1-L2*cos(theta2))*(sqrt(3)/2);Zb2=L2*sin(theta2);
% Xb3=-0.5*(1-L3*cos(theta3));Yb3=(1-L3*cos(theta3))*(sqrt(3)/2);Zb3=L3*sin(theta3);
 
% %Centroid
% Xc=(Xb1+Xb2+Xb3)/3*R;
% Yc=(Yb1+Yb2+Yb3)/3*R;
% Zc=(Zb1+Zb2+Zb3)/3*R;
