close all
clear all
clc

syms xc yc zc %nx ny nz ox oy oz ax ay az 

R=10;r=R;%l=4;dx=4;dy=4;dz=4;zc0=4;
Xc=xc/R; Yc=yc/R;Zc=zc/R;
rho=r/R;

%qd
xc=0;
yc=0;
zc=1;
phi=0;
beta=0;
psi=pi/4;

trans=[xc,yc,zc]';
%Convention XYZ
% mat_R=[cos(alpha)*cos(beta) cos(alpha)*sin(beta)*sin(omega)-sin(alpha)*cos(omega)  cos(alpha)*sin(beta)*cos(omega)+sin(alpha)*sin(omega);
%        sin(alpha)*cos(beta) sin(alpha)*sin(beta)*sin(omega)+cos(alpha)*cos(omega)  sin(alpha)*sin(beta)*cos(omega)-cos(alpha)*sin(omega);
%             -sin(beta)                cos(beta)*sin(omega)             cos(beta)*cos(omega)];

mR=[cos(beta)*cos(psi)               -cos(beta)*sin(psi)               sin(beta);
    cos(phi)*sin(psi)+sin(phi)*sin(beta)*cos(psi)     cos(phi)*cos(psi)-sin(phi)*sin(beta)*sin(psi)     -sin(phi)*cos(beta);
    sin(phi)*sin(psi)-cos(phi)*sin(beta)*cos(psi)     sin(phi)*cos(psi)+cos(phi)*sin(beta)*sin(psi)    cos(phi)*cos(beta)];         
        
nx=mR(1,1);        
ny=mR(2,1);        
nz=mR(3,1);        

ox=mR(1,2);        
oy=mR(2,2);        
oz=mR(3,2);       
        
T=[mR trans;
    0 0 0 1];

L1=sqrt((nx*rho+Xc-1)^2+(ny*rho+Yc)^2+(nz*rho+Zc)^2);
L2=sqrt(0.25*( (-nx*rho+sqrt(3)*ox*rho+2*Xc+1)^2+ (-ny*rho+sqrt(3)*oy*rho+2*Yc-sqrt(3))^2+...
    (-nz*rho+sqrt(3)*oz*rho+2*Zc)^2));
L3=sqrt(0.25*((-nx*rho-sqrt(3)*ox*rho+2*Xc+1)^2+ ...
(-ny*rho-sqrt(3)*oy*rho+2*Yc+sqrt(3))^2 + (-nz*rho-sqrt(3)*oz*rho+2*Zc)^2));
 

L1=subs(L1);
L1=cast(L1,'double');
L2=subs(L2);
L2=cast(L2,'double');
L3=subs(L3);
L3=cast(L3,'double');

l1=L1*R
l2=L2*R
l3=L3*R

%L1-eq
%L2-eq
%L3-eq
%eq de contrainte 6.14 6.15 6.16



% eq1=sqrt((nx+Xc-1)^2+(ny+Yc-1)^2+(nz+Zc-1)^2)-L1;
% eq2=sqrt(0.25*( (-nx+sqrt(3)*ox+2*Xc+1)^2+ (-ny+sqrt(3)*oy+2*Yc-sqrt(3))^2+...
%     (-nz+sqrt(3)*oz+2*Zc)^2))-L2;
% eq3=sqrt(0.25*((-nx-sqrt(3)*ox+2*Xc+1)^2+ ...
% (-ny-sqrt(3)*oy+2*Yc+sqrt(3))^2 + (-nz-sqrt(3)*oz+2*Zc)^2))-L3;
% 
% eq4=ny+Yc==0;
% eq5=ny-ox==0;
% eq6=Xc-(nx-oy)/2==0;

%r=solve(eq1,eq2,eq3,eq4,eq5,eq6,nx,ny,nz,ox,oy,oz)

Xc=subs(Xc);
Xc=cast(Xc,'double');
Yc=subs(Yc);
Yc=cast(Yc,'double');
Zc=subs(Zc);
Zc=cast(Zc,'double');


%x0 = ones(1,6)*10^-3;
% l1=4;l2=0.5;l3=0.4;
% L1=l1/R;L2=l2/R;L3=0.4/R;
% r=fsolve(@(inc)myf(inc,Xc,Yc,Zc,L1,L2,L3),x0)





%% dessin
figure(1)
clf
Pp1=[R;0;0];
plot3(Pp1(1),Pp1(2),Pp1(3),'*k','LineWidth',3)
hold on
text(Pp1(1),Pp1(2),Pp1(3),'  Pp1')

Pp2=[-R/2;R*(sqrt(3)/2);0];
plot3(Pp2(1),Pp2(2),Pp2(3),'*k','LineWidth',3)
hold on
text(Pp2(1),Pp2(2),Pp2(3),'  Pp2')

Pp3=[-R/2;-R*(sqrt(3)/2);0];
plot3(Pp3(1),Pp3(2),Pp3(3),'*k','LineWidth',3)
hold on
text(Pp3(1),Pp3(2),Pp3(3),'  Pp3')

Ppx=[Pp1(1) Pp2(1) Pp3(1) Pp1(1)];
Ppy=[Pp1(2) Pp2(2) Pp3(2) Pp1(2)];
Ppz=[Pp1(3) Pp2(3) Pp3(3) Pp1(3)];

plot3(Ppx,Ppy,Ppz,'Linewidth',3)

Pb1=[r;0;l1];
hold on
plot3(Pb1(1),Pb1(2),Pb1(3),'*k','LineWidth',3)
hold on
text(Pb1(1),Pb1(2),Pb1(3),'  Pb1')

Pb2=[-r/2;r*(sqrt(3)/2);l2];
hold on
plot3(Pb2(1),Pb2(2),Pb2(3),'*k','LineWidth',3)
hold on
text(Pb2(1),Pb2(2),Pb2(3),'  Pb2')

Pb3=[-r/2;-r*(sqrt(3)/2);l3];
hold on
plot3(Pb3(1),Pb3(2),Pb3(3),'*k','LineWidth',3)
hold on
text(Pb3(1),Pb3(2),Pb3(3),'  Pb3')


Pbx=[Pb1(1) Pb2(1) Pb3(1) Pb1(1)];
Pby=[Pb1(2) Pb2(2) Pb3(2) Pb1(2)];
Pbz=[Pb1(3) Pb2(3) Pb3(3) Pb1(3)];

hold on
plot3(Pbx,Pby,Pbz,'g','Linewidth',3)

Z1x=[Pp1(1) Pb1(1)];
Z1y=[Pp1(2) Pb1(2)];
Z1z=[Pp1(3) Pb1(3)];

Z2x=[Pp2(1) Pb2(1)];
Z2y=[Pp2(2) Pb2(2)];
Z2z=[Pp2(3) Pb2(3)];

Z3x=[Pp3(1) Pb3(1)];
Z3y=[Pp3(2) Pb3(2)];
Z3z=[Pp3(3) Pb3(3)];

hold on
plot3(Z1x,Z1y,Z1z,'r','Linewidth',3)
hold on
plot3(Z2x,Z2y,Z2z,'r','Linewidth',3)
hold on
plot3(Z3x,Z3y,Z3z,'r','Linewidth',3)


%% Centre du triangle haut
syms X Y Z
eq1=(Pb3(1)+Pb1(1))/2+(Pb2(1)+Pb1(1))/2+(Pb2(1)+Pb3(1))/2==3*X;
eq2=(Pb3(2)+Pb1(2))/2+(Pb2(2)+Pb1(2))/2+(Pb2(2)+Pb3(2))/2==3*Y;
eq3=(Pb3(3)+Pb1(3))/2+(Pb2(3)+Pb1(3))/2+(Pb2(3)+Pb3(3))/2==3*Z;

solu=solve(eq1,eq2,eq3,X,Y,Z); 
hold on
plot3(solu.X,solu.Y,solu.Z,'*k','Linewidth',3)


%% OT
% c=11;
% syms xd yd zd
% eq10=sqrt((xd-Pb1(1))^2+(yd-Pb1(2))^2+(zd-Pb1(3))^2)-c==0;
% eq20=sqrt((xd-Pb2(1))^2+(yd-Pb2(2))^2+(zd-Pb2(3))^2)-c==0;
% eq30=sqrt((xd-Pb3(1))^2+(yd-Pb3(2))^2+(zd-Pb3(3))^2)-c==0;
% sol=solve(eq10,eq20,eq30,xd,yd,zd); 
% 
% xd=abs(sol.xd);
% yd=abs(sol.yd);
% zd=abs(sol.zd);
% 
% OTx=[solu.X xd(2)];
% OTy=[solu.Y yd(2)];
% OTz=[solu.Z zd(2)];
% 
% hold on
% plot3(OTx,OTy,OTz,'-k','Linewidth',3)
% 
% xlim([-10 10])
% ylim([-10 10])
% zlim([-10 10])

xlabel('X', 'FontSize', 15);
ylabel('Y', 'FontSize', 15);
zlabel('Z', 'FontSize', 15);
axis square

B1=T*[Pb1;1];
B2=T*[Pb2;1];
B3=T*[Pb3;1];

B1=B1(1:3);
B2=B2(1:3);
B3=B3(1:3);

LL1=norm(B1-Pb1);
LL2=norm(B2-Pb2);
LL3=norm(B3-Pb3);

if LL1==l1 && LL2==l2 && LL3==l3
    disp('GG')
else
    disp('NOPE')
end

hold on
plot3(xc,yc,zc,'*r','linewidth',3)



%% Forward Kinematics 
% eq=L1^2+L2^2+3-3*rho^2+L1*L2*cos(theta1)*cos(theta2)-2*L1*L2*sin(theta1)*sin(theta2)-3*L1*cos(theta1)-3*L2*cos(theta2)==0;
% eqq=L2^2+L3^2+3-3*rho^2+L2*L3*cos(theta2)*cos(theta3)-2*L2*L3*sin(theta2)*sin(theta3)-3*L2*cos(theta2)-3*L3*cos(theta3)==0;
% eqqq=L3^2+L1^2+3-3*rho^2+L3*L1*cos(theta3)*cos(theta1)-2*L3*L1*sin(theta3)*sin(theta1)-3*L3*cos(theta3)-3*L1*cos(theta1)==0;
% 
% %Coordinates of ball joints 
% Xb1=1-L1*cos(theta1);Yb1=0;Zb1=L1*sin(theta1);
% Xb2=-0.5*(1-L2*cos(theta2));Yb2=(1-L2*cos(theta2))*(sqrt(3)/2);Zb2=L2*sin(theta2);
% Xb3=-0.5*(1-L3*cos(theta3));Yb3=(1-L3*cos(theta3))*(sqrt(3)/2);Zb3=L3*sin(theta3);
% 
% %Centroid
% Xc=(Xb1+Xb2+Xb3)/3*R;
% Yc=(Yb1+Yb2+Yb3)/3*R;
% Zc=(Zb1+Zb2+Zb3)/3*R;



% %% dessin
% figure(5)
% clf
% Pp1=[R;0;0];
% plot3(Pp1(1),Pp1(2),Pp1(3),'*k','LineWidth',3)
% hold on
% text(Pp1(1),Pp1(2),Pp1(3),'  Pp1')
% 
% Pp2=[-R/2;R*(sqrt(3)/2);0];
% plot3(Pp2(1),Pp2(2),Pp2(3),'*k','LineWidth',3)
% hold on
% text(Pp2(1),Pp2(2),Pp2(3),'  Pp2')
% 
% Pp3=[-R/2;-R*(sqrt(3)/2);0];
% plot3(Pp3(1),Pp3(2),Pp3(3),'*k','LineWidth',3)
% hold on
% text(Pp3(1),Pp3(2),Pp3(3),'  Pp3')
% 
% Ppx=[Pp1(1) Pp2(1) Pp3(1) Pp1(1)];
% Ppy=[Pp1(2) Pp2(2) Pp3(2) Pp1(2)];
% Ppz=[Pp1(3) Pp2(3) Pp3(3) Pp1(3)];
% 
% plot3(Ppx,Ppy,Ppz,'Linewidth',3)
% 
% Pb1=[r;0;LL1];
% hold on
% plot3(Pb1(1),Pb1(2),Pb1(3),'*k','LineWidth',3)
% hold on
% text(Pb1(1),Pb1(2),Pb1(3),'  Pb1')
% 
% Pb2=[-r/2;r*(sqrt(3)/2);LL2];
% hold on
% plot3(Pb2(1),Pb2(2),Pb2(3),'*k','LineWidth',3)
% hold on
% text(Pb2(1),Pb2(2),Pb2(3),'  Pb2')
% 
% Pb3=[-r/2;-r*(sqrt(3)/2);LL3];
% hold on
% plot3(Pb3(1),Pb3(2),Pb3(3),'*k','LineWidth',3)
% hold on
% text(Pb3(1),Pb3(2),Pb3(3),'  Pb3')
% 
% 
% Pbx=[Pb1(1) Pb2(1) Pb3(1) Pb1(1)];
% Pby=[Pb1(2) Pb2(2) Pb3(2) Pb1(2)];
% Pbz=[Pb1(3) Pb2(3) Pb3(3) Pb1(3)];
% 
% hold on
% plot3(Pbx,Pby,Pbz,'g','Linewidth',3)
% 
% Z1x=[Pp1(1) Pb1(1)];
% Z1y=[Pp1(2) Pb1(2)];
% Z1z=[Pp1(3) Pb1(3)];
% 
% Z2x=[Pp2(1) Pb2(1)];
% Z2y=[Pp2(2) Pb2(2)];
% Z2z=[Pp2(3) Pb2(3)];
% 
% Z3x=[Pp3(1) Pb3(1)];
% Z3y=[Pp3(2) Pb3(2)];
% Z3z=[Pp3(3) Pb3(3)];
% 
% hold on
% plot3(Z1x,Z1y,Z1z,'r','Linewidth',3)
% hold on
% plot3(Z2x,Z2y,Z2z,'r','Linewidth',3)
% hold on
% plot3(Z3x,Z3y,Z3z,'r','Linewidth',3)
% 
% hold on
% plot3(xc,yc,zc,'*r','linewidth',3)
% 




%%

%{
eq1=xc+l*ax-dx==0;
eq2=yc+l*ay-dy==0;
eq3=zc-zc0+l*(az-1)==0;

eq4=ny+Yc==0;
eq5=ny-ox==0;
eq6=Xc-(nx-oy)/2==0;

N=[nx;ny;nz];
O=[ox;oy;oz];
A=[ax;ay;az];

eq7=dot(N,N)==1;
eq8=dot(O,O)==1;
eq9=dot(A,A)==1;
eq10=dot(O,A)==0;
eq11=dot(O,N)==0;
eq12=dot(A,N)==0;

r=solve(eq1,eq2,eq3,eq4,eq5,eq6,eq7,eq8,eq9,eq10,eq11,eq12,xc,yc,zc,nx,ny,nz,ox,oy,oz,ax,ay,az)

xc=r.xc
yc=r.yc
zc=r.zc
nx=r.nx
ny=r.ny
nz=r.nz
ox=r.ox
oy=r.oy
oz=r.oz
ax=r.ax
ay=r.ay
az=r.az


L1=sqrt((nx+Xc-1)^2+(ny+Yc-1)^2+(nz+Zc-1)^2);
L2=sqrt(0.25*( (-nx+sqrt(3)*ox+2*Xc+1)^2+ (-ny+sqrt(3)*oy+2*Yc-sqrt(3))^2+...
    (-nz+sqrt(3)*oz+2*Zc)^2));
L3=sqrt(0.25*((-nx-sqrt(3)*ox+2*Xc+1)^2+ ...
(-ny-sqrt(3)*oy+2*Yc+sqrt(3))^2 + (-nz-sqrt(3)*oz+2*Zc)^2));

L11=subs(L1)
L22=subs(L2)
L33=subs(L3)
%}


