close all
clear all
clc

syms xc yc zc

R=10;r=R;
Xc=xc/R; Yc=yc/R;Zc=zc/R;
rho=r/R;

%qd
xc=5;
yc=3;
zc=8;
phi=pi/5;
beta=pi/8;
psi=-pi/12;

trans=[xc,yc,zc]';

mR=[cos(beta)*cos(psi)               -cos(beta)*sin(psi)               sin(beta);
    cos(phi)*sin(psi)+sin(phi)*sin(beta)*cos(psi)     cos(phi)*cos(psi)-sin(phi)*sin(beta)*sin(psi)     -sin(phi)*cos(beta);
    sin(phi)*sin(psi)-cos(phi)*sin(beta)*cos(psi)     sin(phi)*cos(psi)+cos(phi)*sin(beta)*sin(psi)    cos(phi)*cos(beta)];         
        
nx=mR(1,1);        
ny=mR(2,1);        
nz=mR(3,1);        

ox=mR(1,2);        
oy=mR(2,2);        
oz=mR(3,2);       
        
T=[mR trans;
    0 0 0 1];

L1=sqrt((nx*rho+Xc-1)^2+(ny*rho+Yc)^2+(nz*rho+Zc)^2);
L2=sqrt(0.25*( (-nx*rho+sqrt(3)*ox*rho+2*Xc+1)^2+ (-ny*rho+sqrt(3)*oy*rho+2*Yc-sqrt(3))^2+...
    (-nz*rho+sqrt(3)*oz*rho+2*Zc)^2));
L3=sqrt(0.25*((-nx*rho-sqrt(3)*ox*rho+2*Xc+1)^2+ ...
(-ny*rho-sqrt(3)*oy*rho+2*Yc+sqrt(3))^2 + (-nz*rho-sqrt(3)*oz*rho+2*Zc)^2));
 

L1=subs(L1);
L1=cast(L1,'double');
L2=subs(L2);
L2=cast(L2,'double');
L3=subs(L3);
L3=cast(L3,'double');

l1=L1*R
l2=L2*R
l3=L3*R

Xc=subs(Xc);
Xc=cast(Xc,'double');
Yc=subs(Yc);
Yc=cast(Yc,'double');
Zc=subs(Zc);
Zc=cast(Zc,'double');



%% dessin
figure(1)
clf
Pp1=[R;0;0];
plot3(Pp1(1),Pp1(2),Pp1(3),'*k','LineWidth',3)
hold on
text(Pp1(1),Pp1(2),Pp1(3),'  Pp1')

Pp2=[-R/2;R*(sqrt(3)/2);0];
plot3(Pp2(1),Pp2(2),Pp2(3),'*k','LineWidth',3)
hold on
text(Pp2(1),Pp2(2),Pp2(3),'  Pp2')

Pp3=[-R/2;-R*(sqrt(3)/2);0];
plot3(Pp3(1),Pp3(2),Pp3(3),'*k','LineWidth',3)
hold on
text(Pp3(1),Pp3(2),Pp3(3),'  Pp3')

Ppx=[Pp1(1) Pp2(1) Pp3(1) Pp1(1)];
Ppy=[Pp1(2) Pp2(2) Pp3(2) Pp1(2)];
Ppz=[Pp1(3) Pp2(3) Pp3(3) Pp1(3)];

plot3(Ppx,Ppy,Ppz,'Linewidth',3)

Pb1=[r;0;0];
Pb1_ = T*[Pb1;1];
hold on
plot3(Pb1_(1),Pb1_(2),Pb1_(3),'*k','LineWidth',3)
hold on
text(Pb1_(1),Pb1_(2),Pb1_(3),'  Pb1')

Pb2=[-r/2;r*(sqrt(3)/2);0];
Pb2_ = T*[Pb2;1];
hold on
plot3(Pb2_(1),Pb2_(2),Pb2_(3),'*k','LineWidth',3)
hold on
text(Pb2_(1),Pb2_(2),Pb2_(3),'  Pb2')

Pb3=[-r/2;-r*(sqrt(3)/2);0];
Pb3_ = T*[Pb3;1];
hold on
plot3(Pb3_(1),Pb3_(2),Pb3_(3),'*k','LineWidth',3)
hold on
text(Pb3_(1),Pb3_(2),Pb3_(3),'  Pb3')


Pbx=[Pb1_(1) Pb2_(1) Pb3_(1) Pb1_(1)];
Pby=[Pb1_(2) Pb2_(2) Pb3_(2) Pb1_(2)];
Pbz=[Pb1_(3) Pb2_(3) Pb3_(3) Pb1_(3)];

hold on
plot3(Pbx,Pby,Pbz,'g','Linewidth',3)

Z1x=[Pp1(1) Pb1_(1)];
Z1y=[Pp1(2) Pb1_(2)];
Z1z=[Pp1(3) Pb1_(3)];

Z2x=[Pp2(1) Pb2_(1)];
Z2y=[Pp2(2) Pb2_(2)];
Z2z=[Pp2(3) Pb2_(3)];

Z3x=[Pp3(1) Pb3_(1)];
Z3y=[Pp3(2) Pb3_(2)];
Z3z=[Pp3(3) Pb3_(3)];

hold on
plot3(Z1x,Z1y,Z1z,'r','Linewidth',3)
hold on
plot3(Z2x,Z2y,Z2z,'r','Linewidth',3)
hold on
plot3(Z3x,Z3y,Z3z,'r','Linewidth',3)


%% Centre du triangle haut
syms X Y Z
eq1=(Pb3_(1)+Pb1_(1))/2+(Pb2_(1)+Pb1_(1))/2+(Pb2_(1)+Pb3_(1))/2==3*X;
eq2=(Pb3_(2)+Pb1_(2))/2+(Pb2_(2)+Pb1_(2))/2+(Pb2_(2)+Pb3_(2))/2==3*Y;
eq3=(Pb3_(3)+Pb1_(3))/2+(Pb2_(3)+Pb1_(3))/2+(Pb2_(3)+Pb3_(3))/2==3*Z;

solu=solve(eq1,eq2,eq3,X,Y,Z); 
hold on
plot3(solu.X,solu.Y,solu.Z,'*k','Linewidth',3)

hold on
plot3(xc,yc,zc,'*r','linewidth',3)
