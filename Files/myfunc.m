function F=myf(xc,yc,zc,R,inc)

Xc=xc/R; Yc=yc/R;Zc=zc/R;
F(1)=sqrt((inc(1)+Xc-1)^2+(inc(2)+Yc-1)^2+(inc(3)+Zc-1)^2)-L1;
F(2)=sqrt(0.25*( (-inc(1)+sqrt(3)*inc(4)+2*Xc+1)^2+ (-inc(2)+sqrt(3)*inc(5)+2*Yc-sqrt(3))^2+...
    (-inc(3)+sqrt(3)*inc(6)+2*Zc)^2))-L2;
F(3)=sqrt(0.25*((-inc(1)-sqrt(3)*inc(4)+2*Xc+1)^2+ ...
(-inc(2)-sqrt(3)*inc(5)+2*Yc+sqrt(3))^2 + (-inc(3)-sqrt(3)*inc(6)+2*Zc)^2))-L3;

F(4)=inc(2)+Yc;
F(5)=inc(2)-inc(4);
F(6)=Xc-(inc(1)-inc(5))/2;