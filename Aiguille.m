function Aiguille(qd,T)

xc=qd(1);
yc=qd(2);
zc=qd(3);

figure(1)
hold on
plot3(xc,yc,zc,'*r','linewidth',3)


POT=[xc;yc;zc];
POT=T*[POT;1];
figure(1);
hold on
plot3([xc POT(1)],[yc POT(2)],[zc POT(3)],'-k','linewidth',3)
drawnow