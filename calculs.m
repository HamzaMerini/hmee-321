function [L1,L2,L3,T]=calcul(qd,R,r)

rho=r/R;

xc=qd(1);
yc=qd(2);
zc=qd(3);
phi=qd(4);
beta=qd(5);
psi=qd(6);

trans=[xc,yc,zc]';

mR=[cos(beta)*cos(psi)               -cos(beta)*sin(psi)               sin(beta);
    cos(phi)*sin(psi)+sin(phi)*sin(beta)*cos(psi)     cos(phi)*cos(psi)-sin(phi)*sin(beta)*sin(psi)     -sin(phi)*cos(beta);
    sin(phi)*sin(psi)-cos(phi)*sin(beta)*cos(psi)     sin(phi)*cos(psi)+cos(phi)*sin(beta)*sin(psi)    cos(phi)*cos(beta)];
        
nx=mR(1,1);        
ny=mR(2,1);        
nz=mR(3,1);        

ox=mR(1,2);        
oy=mR(2,2);        
oz=mR(3,2);       
        
T=[mR trans;
    0 0 0 1];

Xc=xc/R; Yc=yc/R;Zc=zc/R;

Xc=subs(Xc);
Xc=cast(Xc,'double');
Yc=subs(Yc);
Yc=cast(Yc,'double');
Zc=subs(Zc);
Zc=cast(Zc,'double');

L1=sqrt((nx*rho+Xc-1)^2+(ny*rho+Yc)^2+(nz*rho+Zc)^2);
L2=sqrt(0.25*( (-nx*rho+sqrt(3)*ox*rho+2*Xc+1)^2+ (-ny*rho+sqrt(3)*oy*rho+2*Yc-sqrt(3))^2+...
    (-nz*rho+sqrt(3)*oz*rho+2*Zc)^2));
L3=sqrt(0.25*((-nx*rho-sqrt(3)*ox*rho+2*Xc+1)^2+ ...
(-ny*rho-sqrt(3)*oy*rho+2*Yc+sqrt(3))^2 + (-nz*rho-sqrt(3)*oz*rho+2*Zc)^2));

 
l1=L1*R
l2=L2*R
l3=L3*R


