function pos=MGD(l1,l2,l3,R,r)

f = @(inc)myf(inc,l1,l2,l3,R,r);
pos_initiale = zeros(6,1);
pos = fsolve(f, pos_initiale);
