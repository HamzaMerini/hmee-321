function [tt1,tt2,tt3]=MGD(L1,L2,L3,R,r)
rho=r/R;

%Forward Kinematics %probleme avec solve bloque le programme 

syms t1 t2 t3

eqq1=(L1*L1)+(L2*L2)+3-(3*rho*rho)+(L1*L2*cos(t1)*cos(t2))-(2*L1*L2*sin(t1)*sin(t2))-(3*L1*cos(t1))-(3*L2*cos(t2))==0;
eqq2=(L2*L2)+(L3*L3)+3-(3*rho*rho)+(L2*L3*cos(t2)*cos(t3))-(2*L2*L3*sin(t2)*sin(t3))-(3*L2*cos(t2))-(3*L3*cos(t3))==0;
eqq3=(L3*L3)+(L2*L2)+3-(3*rho*rho)+(L3*L1*cos(t3)*cos(t1))-(2*L3*L1*sin(t3)*sin(t1))-(3*L3*cos(t3))-(3*L1*cos(t1))==0;
[t1,t2,t3]=solve(eqq1,eqq2,eqq3,[t1 t2 t3],'Real',true,'PrincipalValue',true)% cest ca qui fou la merde a hamza


t1=abs(t1)
t2=abs(t2)
t3=abs(t3)

 %Coordinates of ball joints 
    Xb1=1-L1*cos(t1);
    Yb1=0;
    Zb1=L1*sin(t1);
    figure(2)
    hold on
    plot3(Xb1,Yb1,Zb1,'*y','linewidth',5)
    text(Xb1,Yb1,Zb1,'  Pb1')

    Xb2=-0.5*(1-L2*cos(t2));
    Yb2=(sqrt(3)/2)*(1-L2*cos(t2));
    Zb2=L2*sin(t2);
    hold on
    plot3(Xb2,Yb2,Zb2,'*y','linewidth',5)
    text(Xb2,Yb2,Zb2,'  Pb2')

    Xb3=-0.5*(1-L3*cos(t3));
    Yb3=-(sqrt(3)/2)*(1-L3*cos(t3));
    Zb3=L3*sin(t3);
    hold on
    plot3(Xb3,Yb3,Zb3,'*y','linewidth',5)
    text(Xb3,Yb3,Zb3,'  Pb3')

    %Position of centroid
    Xc=abs((Xb1+Xb2+Xb3)/(R*3));
    Yc=abs((Yb1+Yb2+Yb3)/(R*3));
    Zc=abs((Zb1+Zb2+Zb3)/(R*3));

    hold on
    plot3([Xb1 Xb2 Xb3 Xb1],[Yb1 Yb2 Yb3 Yb1],[Zb1 Zb2 Zb3 Zb1],'-k','Linewidth',3)

    hold on
    plot3(Xc*R,Yc*R,Zc*R,'*m','linewidth',5)
    grid on
    view(-18,27)


    Pp1=[R;0;0];
    Pp2=[-R/2;R*(sqrt(3)/2);0];
    Pp3=[-R/2;-R*(sqrt(3)/2);0];

    Ppx=[Pp1(1) Pp2(1) Pp3(1) Pp1(1)];
    Ppy=[Pp1(2) Pp2(2) Pp3(2) Pp1(2)];
    Ppz=[Pp1(3) Pp2(3) Pp3(3) Pp1(3)];

   

 plot3(Ppx/R,Ppy/R,Ppz/R,'r','Linewidth',3)


xlabel('X', 'FontSize', 15);
ylabel('Y', 'FontSize', 15);
zlabel('Z', 'FontSize', 15);

tt1=t1;
tt2=t2;
tt3=t3;
