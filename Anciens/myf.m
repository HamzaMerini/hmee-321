function F=myf(inc,l1,l2,l3,R,r)

rho=r/R;

L1 = l1/R;      
L2 = l2/R;
L3 = l3/R;

xc = inc(1);   
yc = inc(2);     
zc = inc(3);     
phi = inc(4);
beta = inc(5);
psi = inc(6);

mR=[cos(beta)*cos(psi)               -cos(beta)*sin(psi)               sin(beta);
    cos(phi)*sin(psi)+sin(phi)*sin(beta)*cos(psi)     cos(phi)*cos(psi)-sin(phi)*sin(beta)*sin(psi)     -sin(phi)*cos(beta);
    sin(phi)*sin(psi)-cos(phi)*sin(beta)*cos(psi)     sin(phi)*cos(psi)+cos(phi)*sin(beta)*sin(psi)    cos(phi)*cos(beta)];        
                
        
nx=mR(1,1);        
ny=mR(2,1);        
nz=mR(3,1);        

ox=mR(1,2);        
oy=mR(2,2);        
oz=mR(3,2);       

% nx = cos(beta)*cos(psi);
% ny = cos(phi)*sin(psi)+cos(psi)*sin(phi)*sin(beta);
% nz = sin(phi)*sin(psi)-cos(phi)*cos(psi)*sin(beta);
% 
% ox = -cos(beta)*sin(psi);
% oy = cos(phi)*cos(psi)-sin(phi)*sin(beta)*sin(psi);
% oz = cos(psi)*sin(phi)+cos(phi)*sin(beta)*sin(psi);


Xc=xc/R; Yc=yc/R;Zc=zc/R;

F(1)=(nx*rho+Xc-1)^2+(ny*rho+Yc)^2+(nz*rho+Zc)^2- L1^2;
F(2)=0.25*((-nx*rho+sqrt(3)*ox*rho+2*Xc+1)^2+ (-ny*rho+sqrt(3)*oy*rho+2*Yc-sqrt(3))^2+...
    (-nz*rho+sqrt(3)*oz*rho+2*Zc)^2)-L2^2;
F(3)=0.25*((-nx*rho-sqrt(3)*ox*rho+2*Xc+1)^2+ ...
(-ny*rho-sqrt(3)*oy*rho+2*Yc+sqrt(3))^2 + (-nz*rho-sqrt(3)*oz*rho+2*Zc)^2)-L3^2;

F(4)=(ny*rho)+Yc;
F(5)=ny-ox;
F(6)=((rho/2)*(nx-oy))-Xc;

