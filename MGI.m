function pos=MGI(l1,l2,l3,R,r)

f = @(inc)equations(inc,l1,l2,l3,R,r);
pos_initiale = zeros(6,1);
pos = fsolve(f, pos_initiale);
