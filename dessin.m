function dessin(T,R,r)

figure(1)
clf
Pp1=[R;0;0];
plot3(Pp1(1),Pp1(2),Pp1(3),'*k','LineWidth',3)
hold on
text(Pp1(1),Pp1(2),Pp1(3),'  Pp1')

Pp2=[-R/2;R*(sqrt(3)/2);0];
plot3(Pp2(1),Pp2(2),Pp2(3),'*k','LineWidth',3)
hold on
text(Pp2(1),Pp2(2),Pp2(3),'  Pp2')

Pp3=[-R/2;-R*(sqrt(3)/2);0];
plot3(Pp3(1),Pp3(2),Pp3(3),'*k','LineWidth',3)
hold on
text(Pp3(1),Pp3(2),Pp3(3),'  Pp3')

Ppx=[Pp1(1) Pp2(1) Pp3(1) Pp1(1)];
Ppy=[Pp1(2) Pp2(2) Pp3(2) Pp1(2)];
Ppz=[Pp1(3) Pp2(3) Pp3(3) Pp1(3)];

plot3(Ppx,Ppy,Ppz,'Linewidth',3)

Pb1=[r;0;0];
Pb1_=T*[Pb1;1];
hold on
plot3(Pb1_(1),Pb1_(2),Pb1_(3),'*k','LineWidth',3)
hold on
text(Pb1_(1),Pb1_(2),Pb1_(3),'  Pb1')

Pb2=[-r/2;r*(sqrt(3)/2);0];
Pb2_=T*[Pb2;1];
hold on
plot3(Pb2_(1),Pb2_(2),Pb2_(3),'*k','LineWidth',3)
hold on
text(Pb2_(1),Pb2_(2),Pb2_(3),'  Pb2')

Pb3=[-r/2;-r*(sqrt(3)/2);0];
Pb3_=T*[Pb3;1];
hold on
plot3(Pb3_(1),Pb3_(2),Pb3_(3),'*k','LineWidth',3)
hold on
text(Pb3_(1),Pb3_(2),Pb3_(3),'  Pb3')


Pbx=[Pb1_(1) Pb2_(1) Pb3_(1) Pb1_(1)];
Pby=[Pb1_(2) Pb2_(2) Pb3_(2) Pb1_(2)];
Pbz=[Pb1_(3) Pb2_(3) Pb3_(3) Pb1_(3)];

hold on
plot3(Pbx,Pby,Pbz,'g','Linewidth',3)

Z1x=[Pp1(1) Pb1_(1)];
Z1y=[Pp1(2) Pb1_(2)];
Z1z=[Pp1(3) Pb1_(3)];

Z2x=[Pp2(1) Pb2_(1)];
Z2y=[Pp2(2) Pb2_(2)];
Z2z=[Pp2(3) Pb2_(3)];

Z3x=[Pp3(1) Pb3_(1)];
Z3y=[Pp3(2) Pb3_(2)];
Z3z=[Pp3(3) Pb3_(3)];

hold on
plot3(Z1x,Z1y,Z1z,'r','Linewidth',3)
hold on
plot3(Z2x,Z2y,Z2z,'r','Linewidth',3)
hold on
plot3(Z3x,Z3y,Z3z,'r','Linewidth',3)

title("Simulation : Mouvement du Robot Micron")


%% Centre du triangle haut
syms X Y Z
eq1=(Pb3_(1)+Pb1_(1))/2+(Pb2_(1)+Pb1_(1))/2+(Pb2_(1)+Pb3_(1))/2==3*X;
eq2=(Pb3_(2)+Pb1_(2))/2+(Pb2_(2)+Pb1_(2))/2+(Pb2_(2)+Pb3_(2))/2==3*Y;
eq3=(Pb3_(3)+Pb1_(3))/2+(Pb2_(3)+Pb1_(3))/2+(Pb2_(3)+Pb3_(3))/2==3*Z;

solu=solve(eq1,eq2,eq3,X,Y,Z); 
hold on
plot3(solu.X,solu.Y,solu.Z,'*k','Linewidth',6)


xlabel('X', 'FontSize', 15);
ylabel('Y', 'FontSize', 15);
zlabel('Z', 'FontSize', 15);

grid on
xlim([-15 15]*1e-5) 
ylim([-15 15]*1e-5) 
zlim([0 30]*1e-5) 
%view(-37,20)
view([1,1,1]);